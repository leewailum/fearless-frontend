import React, {useEffect, useState} from 'react';

function ConferenceForm(props) {

  const [name, setName] = useState('');
  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const [starts, setStarts] = useState('');
  const handleStartsChange = (event) => {
    const value = event.target.value;
    setStarts(value);
  }

  const [ends, setEnds] = useState('');
  const handleEndsChange = (event) => {
    const value = event.target.value;
    setEnds(value);
  }

  const [description, setDescription] = useState('');
  const handleDescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
  }

  const [maxPresentations, setMaxPresentations] = useState('');
  const handleMaxPresentationsChange = (event) => {
    const value = event.target.value;
    setMaxPresentations(value);
  }

  const [maxAttendees, setMaxAttendees] = useState('');
  const handleMaxAttendeesChange = (event) => {
    const value = event.target.value;
    setMaxAttendees(value);
  }

  const [locations, setLocations] = useState([]);

  const [location, setLocation] = useState('');
  const handleLocationChange = (event) => {
    const value = event.target.value;
    setLocation(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.name = name;
    data.starts = starts;
    data.ends = ends;
    data.description = description;
    data.max_presentations = maxPresentations;
    data.max_attendees = maxAttendees;
    data.location = location;

    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
      console.log(response);
    if (response.ok) {
      const newConferences = await response.json();
      console.log(newConferences);

      setName('');
      setStarts('');
      setEnds('');
      setDescription('');
      setMaxPresentations('');
      setMaxAttendees('');
      setLocation('');
    }
  }


  const fetchData = async () => {
    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return(
    <div className="my-5 container">
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" ></input>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartsChange} value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"></input>
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndsChange} value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"></input>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="description">Description</label>
                            <textarea onChange={handleDescriptionChange} value={description} placeholder="Description" required type="text" name="description" id="description" className="form-control" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                            <label htmlFor="max_presentations">Max presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                            <label htmlFor="max_attendees">Max attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                        return (
                                            <option key={location.href} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
  )
}

export default ConferenceForm;
